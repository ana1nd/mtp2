#for multiple languages 

lang_pair=$1
num_lines=$2
reference_file=baselines/tokenized/references/*.$1
hypo_dir=baselines/tokenized/system-outputs/newstest2014/$1

#for any single language pair 
#reference_file=baselines/tokenized/references/*.hi-en
#hypo_dir=baselines/tokenized/system-outputs/newstest2014/hi-en

arr=()
for file_ in $hypo_dir/*;do
	temp1=${file_%*/}
	temp2=${temp1##*/}
	
	arr+=($temp1)
	#break #currently for just one file
done
#echo ${arr[@]}
python score.py $reference_file ${arr[@]}

#run awk script to find average BLEU score of a system by summing over number of lines and dividing by number of lines
#awk '{s+=$4}NR%2507==0{print s/2507;s=0}' score.hi-en.txt > a.txt
awk -v num=$2 '{s+=$4}NR%num==0{print s/num;s=0}' WE-metric.score > average_WE-metric.score


#command to run segment correlation
#cat WE-metric.score > temp_file.score

sed -e "s/^/word_embedding\t$1\t/" WE-metric.score > myWE-metric.score

#run this script to fetch human score for any language pair from baselines/human*
awk -v language_pair=$1 '$2 == language_pair {print $5}' human-2014-05-16.scores > human_metric.score

echo "<--------------->"
echo $1,"System Correlation"
python pearson.py average_WE-metric.score human_metric.score
echo "<--------------->"

echo $1,"Segment Correlation"
python temp_segment.py myWE-metric.score $1
echo "<--------------->"
#rm *.score



#de-en : 3003 (number of lines in 1 system translation)
#hi-en : 2507

