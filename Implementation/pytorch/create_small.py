import unicodedata
import re


def unicodeToAscii(s):
    return ''.join(
        c for c in unicodedata.normalize('NFD', s)
        if unicodedata.category(c) != 'Mn'
    )

def normalizeString(s):
    s = unicodeToAscii(s.lower().strip())
    s = re.sub(r"([.!?])", r" \1", s)
    s = re.sub(r"[^0-9a-zA-Z.!?]+", r" ", s)
    return s


f = open('data/news.2013.en.shuffled','r')
lines = f.readlines()
f2 = open('data/vocab','w')
mx = -1
dt = dict()
wordCounter = dict()
vocab = set()
numLines = 50000 #len(lines)
for i in xrange(0,numLines,1):
    line = lines[i]
    line = normalizeString(line.decode('utf-8'))
    words = line.split(' ')            
    nwords = len(words)
    if nwords > 50 or nwords < 10:
        continue
    
    for w in words:
        if w in wordCounter:
            wordCounter[w] += 1
        else:
            wordCounter[w] = 1

    vocab.update(words)
    if nwords in dt.keys():
        dt[nwords] += 1
    else:
        dt[nwords] = 1
    
    line_ = line + "\n"
    #f2.write(line_)
    
    if i % 10000 == 0:
        print i,line,len(vocab)


'''f2 = open('data/eng-eng-10.txt','a+')
lines = open('data/news.small').read().strip().split('\n')
lines_ = [normalizeString(a) for a in lines]

count = 0
z = zip(lines_,lines_)
for a,b in z:
    #print count
    count += 1
    line_ = "{}\t{}".format(a,b) + "\n"
    f2.write(unicode(line_))
    
lines = open('data/eng-eng-100.txt', encoding='utf-8').read().strip().split('\n')
pairs = [[normalizeString(s) for s in l.split('\t')] for l in lines]'''
    