from collections import Counter
import unicodedata
import re
import os
import pickle


def unicodeToAscii(s):
    return ''.join(
        c for c in unicodedata.normalize('NFD', s)
        if unicodedata.category(c) != 'Mn'
    )

def normalizeString(s):
    s = unicodeToAscii(s.lower().strip())
    s = re.sub(r"([.!?])", r" \1", s)
    s = re.sub(r"[^0-9a-zA-Z.!?]+", r" ", s)
    return s

if __name__ == '__main__':
    
    f = open('data/news.2013.en','r')
    f2 = open('data/news.small.data','w+')
    lines = f.readlines()
    totalLines = len(lines)
    numLines = 5*pow(10,5)
    vocab_list = []
    wordCounter = Counter()
    count = 0
    printEvery = 10000
    
    for i in range(0,numLines,1):
        line = lines[i]
        line = normalizeString(line.decode('utf-8'))
        words = line.split(' ')            
        nwords = len(words)
        if nwords > 50 or nwords < 10:
            continue
        line_ = line + "\n"
        f2.write(line_)
        if i % printEvery == 0:
            print i
    
    f2.close()
    f.close()
    
    lines_ = open('data/news.small.data','r').read().strip().split('\n')
    f2 = open('data/news.tab.data','w')
    z = zip(lines_,lines_)
    print("Creating tabbed data")
    printEvery2 = 100000
    for i in range(0,len(z),1):
        a,b = z[i]
        line_ = "{}\t{}".format(a,b) + "\n"
        f2.write(unicode(line_))
        if i % printEvery2 == 0:
            print(i,a,b)
    
    lines_ = open('data/news.tab.data').read().strip().split('\n')
    
