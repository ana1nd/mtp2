from collections import Counter
import unicodedata
import re
import os
import pickle


def unicodeToAscii(s):
    return ''.join(
        c for c in unicodedata.normalize('NFD', s)
        if unicodedata.category(c) != 'Mn'
    )

def normalizeString(s):
    s = unicodeToAscii(s.lower().strip())
    s = re.sub(r"([.!?])", r" \1", s)
    s = re.sub(r"[^0-9a-zA-Z.!?]+", r" ", s)
    return s


if __name__ == '__main__':
    
    f = open('data/news.2013.en','r')
    lines = f.readlines()
    totalLines = len(lines)
    numLines = totalLines
    vocab_list = []
    wordCounter = Counter()
    count = 0
    printEvery = 100000
    
    for i in range(0,numLines,1):
        line = lines[i]
        line = normalizeString(line.decode('utf-8'))
        words = line.split(' ')            
        nwords = len(words)
        if nwords > 50 or nwords < 10:
            continue
        count += 1
        wordCounter.update(words)
        if i % printEvery == 0:
            print i,count
    
    
    path = os.path.join('model','vocab.pkl')
    with open(path, 'wb') as output:
        pickle.dump(wordCounter, output, pickle.HIGHEST_PROTOCOL)
    
    