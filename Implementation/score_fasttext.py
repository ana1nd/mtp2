if __name__ == '__main__':
    
    arguments = sys.argv
    script = arguments[0]
    reference_file = arguments[1]
    num_lines = arguments[2]
    num_lines = int(num_lines)
    system_translation =  arguments[3:]
    vector_path = os.path.join("pretrained","wiki.en.bin")
    word_vectors = KeyedVectors.load_word2vec_format(vector_path, binary=True)  # C binary format
    #print script,reference_file,system_translation
    for  i in system_translation :
        myLine = "<=========================================>" + "\n" 
        f2.write(myLine)
        f3.write(myLine)
        myLine2 = i + "\n"
        f2.write(myLine2)
        f2.write(myLine)
        f3.write(myLine2)
        f3.write(myLine)
        main(word_vectors,num_lines,reference_file,i,f)
    print ("Total Words = %d,Hit Words = %d, Ratio=%f "%(total_words,hit_words,hit_words/total_words))
    myLine = "<------------------------------------------------------------------------------------------------->" + "\n"
    f2.write(myLine)
    f3.write(myLine)
    f2.close()
    f3.close()
    f.close()
    
    f_simple = open('simple_baseline.score','r')
    f_avg = open("avg_baseline.score","a+")
    lines = f_simple.readlines()
    par = len(lines)/num_lines
    for i in range(0,int(par),1):
        start = i*num_lines
        end = start + num_lines
        line = lines[start]
        line = line.split('\t')
        sys = line[1]
        sys_front = sys[0:sys.rfind('.')]
        sys_back = sys[sys.rfind('.')+1:]
        score = 0
        for j in range(start,end,1):
            score = score + float(lines[j].split('\t')[3])
        avg = score/num_lines
        print (start,end,sys_front,sys_back,avg,score)
        line_write = "baseline" + "\t" + sys_back + "\t" + line[0] + "\t" + sys_front +  "\t" + str(avg) + "\n"
        f_avg.write(line_write)
        print(line_write)    
    f_avg.close()
    f_simple.close()
    f4.close()