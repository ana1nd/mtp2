from __future__ import division
from gensim.models.keyedvectors import KeyedVectors
import sys
import unicodedata
import numpy as np
import pickle
from math import *
import scipy.spatial.distance as distance

f = open("WE-metric.score","a+")
def findSimilarity(ref_dt,hyp_dt):
    
    dictionary = dict()    
    for k in hyp_dt.keys():
        
        vec = hyp_dt[k]
        mx = -99999999
        for l in ref_dt.keys():
            ref_vec = ref_dt[l]
            
            dist = distance.cosine(vec,ref_vec)
            #print k,similarity,l
            similarity = 1 - dist
            #print k,similarity,l
            if similarity > mx :
                mx = similarity
                mx_str = l 
        
        #print k,mx,mx_str
        words = k.split(" ")
        w = len(words)
        if w in dictionary.keys():
            tp = dictionary[w]
            temp = (tp[0]+mx,tp[1]+1)
            dictionary[w] = temp
        else:
            tp = (mx,1)
            dictionary[w] = tp
        
    #print dictionary
    return dictionary

def main(word_vectors,reference_file,system_translation,f):
    
    hyp_words = []
    hit = []
    miss = [] 

    #print "Begin"
    ref = open(reference_file,"r")
    ref_data = ref.readlines()    
    #sys = open(system_translation,"r")

    it = 0
    hyp = open(system_translation,"r")
    hyp_data = hyp.readlines()
            
    for line_num in range(0,len(ref_data),1):
        it+= 1
        
        ref_line = ref_data[line_num]        
        ref_ls = ref_line.split(" ")        
        ref_ls[-1]=ref_ls[-1][0:len(ref_ls[-1])-1]
        
        #print ref_ls
        
        ref_dt = dict()
        hyp_dt = dict()
        for i in range(1,5,1): #till 4grams words
                        
            hyp_line = hyp_data[line_num] #using the same variable of ref since line numbers are equal
            hyp_ls = hyp_line.split(" ")
            hyp_ls[-1]=hyp_ls[-1][0:len(hyp_ls[-1])-1]
            
            for i_ in range(0,len(hyp_ls),1):
                hyp_ls[i_] = unicodedata.normalize('NFKD',unicode(hyp_ls[i_],'utf8')).encode('ascii','ignore')

            #print hyp_ls
            score = 0
            for j in range(0,len(ref_ls)-i+1,1):
                ref_ngram = ref_ls[j:j+i]
                #print ref_ngram
                word_wv = np.zeros((300))
                flag = False
                for each_word in ref_ngram:
                    if each_word in word_vectors:
                        word_wv += word_vectors[each_word]
                        flag = True
                if flag == True:
                    ref_dt[" ".join(ref_ngram)] = word_wv/len(ref_ngram)
                
            for k in range(0,len(hyp_ls)-i+1,1):
                hyp_ngram = hyp_ls[k:k+i]
                if i==1:
                    hyp_words.append(hyp_ngram)
                    
                #print hyp_ngram
                word_wv = np.zeros((300))
                flag = False
                for each_word in hyp_ngram:
                    if each_word in word_vectors:
                        if i==1:
                            hit.append(each_word)
                        word_wv += word_vectors[each_word]
                        flag = True
                    else:
                        if i==1:
                            miss.append(each_word)
                #print " ".join(hyp_ngram),len(word_wv),len(hyp_ngram)
                if flag==True:
                    hyp_dt[" ".join(hyp_ngram)] = word_wv/len(hyp_ngram)
                
        dictionary = findSimilarity(ref_dt,hyp_dt)
        w = 1/4
        ref_len = len(ref_ls)
        hyp_len = len(hyp_ls)
        score = 1
        keys_ls = dictionary.keys()
        for n in range(0,len(keys_ls),1):
            precision = dictionary[keys_ls[n]][0]/dictionary[keys_ls[n]][1]
            if precision<0:
                precision = 0
            score = score*pow(precision,w)
            #print n,precision,score
        if ref_len >= hyp_len :
            BLEU = (pow(e,1-ref_len/hyp_len)) * (score)
        else:
            BLEU = score
        line_ = system_translation[system_translation.rfind("/")+1:]
        data_dir = line_[0:line_.find(".")]
        sys_id = line_[line_.find(".")+1:]
        line_ = data_dir + "\t" + sys_id + "\t" + str(line_num+1) + "\t" + str(BLEU)  +"\n"
        f.write(line_)
            #print it,BLEU
        '''if it%1000==0:
            print it,BLEU'''
    #print "Done"
    total_words_hyp = [item for sublist in hyp_words for item in sublist]
    return total_words_hyp,hit,miss
                        

if __name__ == '__main__':
    
    words_ls = []
    hit_ls = []
    miss_ls = []
    arguments = sys.argv
    script = arguments[0]
    reference_file = arguments[1]
    system_translation =  arguments[2:]
    word_vectors = KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin', binary=True)  # C binary format
    #print script,reference_file,system_translation
    for  i in system_translation :
        print i
        
        hyp_words,hit,miss = main(word_vectors,reference_file,i,f)
        hyp_words_ = set(hyp_words)
        hit_ = set(hit)
        miss_ = set(miss)
        print len(hyp_words),len(hit),len(miss),len(hyp_words_),len(hit_),len(miss_),len(hit_)/len(hyp_words_)
    f.close()
    
    
    
    
