#this code runs for each language pair (more specifically *-en (to english pair))

lang_dir=baselines/tokenized/system-outputs/newstest2014/
rm -rf submissions/EMBEDDING

arr=()

for file_ in $lang_dir/*en;do
	temp1=${file_%*/}
	temp2=${temp1##*/}
	
	arr+=($temp1)
	a=($(wc $lang_dir/$temp2/*))
	lines=${a[0]}
	echo $temp2,$lines				
	bash iterator_baseline.sh $temp2 $lines
	break #for only one language pair
done
#print $arr
mv avg_baseline.score baseline.sys.score
mkdir submissions/EMBEDDING
mv baseline.sys.score submissions/EMBEDDING
mv baseline.seg.score submissions/EMBEDDING

make -f Makefile
mkdir -p results
mv segment* results/
mv system* results/

#rm *.score
#rm *.sentence


