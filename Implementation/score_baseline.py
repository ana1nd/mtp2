from __future__ import division
from gensim.models.keyedvectors import KeyedVectors
import sys
import unicodedata
import scipy.spatial.distance as distance
from operator import add
import numpy as np
import string
import os
np.seterr(divide='ignore', invalid='ignore')

total_words = 0
hit_words = 0
f = open("simple_baseline.score","w")
f2 = open("goodSimilarity.baseline.sentence","a+")
f3 = open("badSimilarity.baseline.sentence","a+")
f4 = open("baseline.seg.score","a+")
def main(word_vectors,num_lines,reference_file,system_translation,f):
    #print "Begin"
    ref = open(reference_file,"r")
    ref_data = ref.readlines()    
    #sys = open(system_translation,"r")

    it = 0
    hyp = open(system_translation,"r")
    hyp_data = hyp.readlines()
            
    for line_num in range(0,len(ref_data),1):
        it+= 1
        
        ref_line = ref_data[line_num]   
        orig_ref_line = ref_line
        ref_line = unicodedata.normalize('NFKD',unicode(ref_line,'utf8')).encode('ascii','ignore')
        ref_line = ref_line.translate(None,string.punctuation)
        ref_ls = ref_line.split(" ")        
        ref_ls[-1]=ref_ls[-1][0:len(ref_ls[-1])-1]
        
        hyp_line = hyp_data[line_num] #using the same variable of ref since line numbers are equal
        orig_hyp_line = hyp_line
        hyp_line = unicodedata.normalize('NFKD',unicode(hyp_line,'utf8')).encode('ascii','ignore')
        hyp_line = hyp_line.translate(None,string.punctuation)
        hyp_ls = hyp_line.split(" ")
        hyp_ls[-1]=hyp_ls[-1][0:len(hyp_ls[-1])-1]
        
#        for i_ in range(0,len(hyp_ls),1):
#            hyp_ls[i_] = unicodedata.normalize('NFKD',unicode(hyp_ls[i_],'utf8')).encode('ascii','ignore')
        
        count1 = 0
        hyp_word_vector = [0 for i in range(0,300,1)]
        global total_words
        total_words += len(hyp_ls)
        for word in hyp_ls:
            if word in word_vectors:
                hyp_word_vector = map(add,hyp_word_vector,word_vectors[word])
                count1 = count1 + 1
                #print word                
        count2 = 0
        ref_word_vector = [0 for i in range(0,300,1)]
        for word in ref_ls:
            if word in word_vectors:
                ref_word_vector = map(add,ref_word_vector,word_vectors[word])
                count2 = count2 + 1
                
        global  hit_words
        hit_words += count1
                #print word
        if count1!=0 and count2!=0:
            average_hyp = map(lambda x:x/count1,hyp_word_vector)
            average_ref = map(lambda x:x/count2,ref_word_vector)        
            dist = distance.cosine(average_hyp,average_ref)
        else:
            dist = 1

        similarity = 1 - dist
        score = similarity
        
        if score > 0.90:
            myLine = str(line_num+1) + "\t" + orig_ref_line + "\t" + orig_hyp_line + "\t" + str(score) + "\n"
            f2.write(myLine)
        
        if score <= 0.20:
            myLine2 = str(line_num+1) + "\t" + orig_ref_line + "\t" + orig_hyp_line + "\t" + str(score) + "\n"
            f3.write(myLine2)
        
        line_ = system_translation[system_translation.rfind("/")+1:]
        data_dir = line_[0:line_.find(".")]
        sys_id = line_[line_.find(".")+1:]
        sys_name = sys_id[0:sys_id.rfind('.')]
        lang_pair = sys_id[sys_id.rfind('.')+1:]
        line_ = data_dir + "\t" + sys_id + "\t" + str(line_num+1) + "\t" + str(score)  +"\n"
        line__ = "baseline" + "\t" + lang_pair + "\t" + data_dir + "\t"  + sys_name + "\t"  + str(line_num+1) + "\t" + str(score)  +"\n"
        f.write(line_)
        f4.write(line__)
    #print "Done"                    

if __name__ == '__main__':
    
    arguments = sys.argv
    script = arguments[0]
    reference_file = arguments[1]
    num_lines = arguments[2]
    num_lines = int(num_lines)
    system_translation =  arguments[3:]
    vector_path = os.path.join("pretrained","GoogleNews-vectors-negative300.bin")
    word_vectors = KeyedVectors.load_word2vec_format(vector_path, binary=True)  # C binary format
    #print script,reference_file,system_translation
    for  i in system_translation :
        myLine = "<=========================================>" + "\n" 
        f2.write(myLine)
        f3.write(myLine)
        myLine2 = i + "\n"
        f2.write(myLine2)
        f2.write(myLine)
        f3.write(myLine2)
        f3.write(myLine)
        main(word_vectors,num_lines,reference_file,i,f)
    print ("Total Words = %d,Hit Words = %d, Ratio=%f "%(total_words,hit_words,hit_words/total_words))
    myLine = "<------------------------------------------------------------------------------------------------->" + "\n"
    f2.write(myLine)
    f3.write(myLine)
    f2.close()
    f3.close()
    f.close()
    
    f_simple = open('simple_baseline.score','r')
    f_avg = open("avg_baseline.score","a+")
    lines = f_simple.readlines()
    par = len(lines)/num_lines
    for i in range(0,int(par),1):
        start = i*num_lines
        end = start + num_lines
        line = lines[start]
        line = line.split('\t')
        sys = line[1]
        sys_front = sys[0:sys.rfind('.')]
        sys_back = sys[sys.rfind('.')+1:]
        score = 0
        for j in range(start,end,1):
            score = score + float(lines[j].split('\t')[3])
        avg = score/num_lines
        print (start,end,sys_front,sys_back,avg,score)
        line_write = "baseline" + "\t" + sys_back + "\t" + line[0] + "\t" + sys_front +  "\t" + str(avg) + "\n"
        f_avg.write(line_write)
        print(line_write)    
    f_avg.close()
    f_simple.close()
    f4.close()
    
    
    
