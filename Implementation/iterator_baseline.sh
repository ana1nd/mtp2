#for multiple languages 
lang_pair=$1
num_lines=$2
reference_file=baselines/tokenized/references/*.$1
hypo_dir=baselines/tokenized/system-outputs/newstest2014/$1

#for any single language pair 
#reference_file=baselines/tokenized/references/*.cs-en
#hypo_dir=baselines/tokenized/system-outputs/newstest2014/cs-en

arr=()
for file_ in $hypo_dir/*;do
	temp1=${file_%*/}
	temp2=${temp1##*/}
	
	arr+=($temp1)
	echo $1,$temp1,$2
	#break #currently for just one file in *-en language pair
done

#echo ${arr[@]}

#run python script to find setence-wise BLEU score for a language pair
python score_baseline.py $reference_file $num_lines ${arr[@]}


#run awk script to find average BLEU score of a system by summing over number of lines and dividing by number of lines
#awk '{s+=$4}NR%3003==0{print s/3003;s=0}' BLEU-score.de-en.txt > a.txt
#awk -v num=$2 '{s+=$4}NR%num==0{print s/num;s=0}' simple_baseline.score > average_baseline.score

#command to run segment correlation
#cat simple_baseline.score > temp_file.score
#sed -e "s/^/simple\t$1\t/" temp_file.score > 	mySimple_baseline.score

#run this script to fetch human score for any language pair from baselines/human*
#awk -v language_pair=$1 '$2 == language_pair {print $5}' human-2014-05-16.scores > human-metric.score

#exit 1;
#echo "<--------------->"
#echo $1,"System Correlation"
#python pearson.py average_baseline.score human-metric.score
#echo "<--------------->"

#echo $1,"Segment Correlation"
#python temp_segment.py mySimple_baseline.score $1
#echo "<--------------->"




#de-en : 3003 (number of lines in 1 system translation)
#hi-en : 2507
